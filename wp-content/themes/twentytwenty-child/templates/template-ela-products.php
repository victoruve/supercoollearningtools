<?php
/**
 * Template Name: ELA Template Products
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */
get_header('new');
 
?>

<div class="breadcrumb">
	<div class="breadcrumb-container">
		<ul id="breadcrumbs" class="flat-list">
        	<li><a href="<?php echo home_url(); ?>">Home</a></li>
            <li class="separator">/</li>
			<li><a href="<?php echo home_url(); ?>/lessons/">Curriculum</a></li>
            <li class="separator">/</li>    
            <li><strong class="bread-current"><?php echo the_title(); ?></strong></li>
        </ul>
        
    </div>
</div>

<!-- .nav-toggle -->

			 
 

				<!-- custom-page-menu -->
 

<main id="site-content" role="main" style="clear:both" class="ela-page">

<div class="main-content">
	<div class="products-part">


<div class="top-content">
	
    
    <?php

if ( have_posts() ) {

	while ( have_posts() ) {
		the_post();
?>
		<h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
<?php	}
}

?>

</div>











	<div class="readings-box">
<?php
$args = array(
    'post_type'=> 'ela',
    'order'    => 'ASC',
	'posts_per_page' => 12
    );
$the_query = new WP_Query( $args );
if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 

?>


	<div class="blog-section-common">
    <div class="blog-image"><a href="<?php echo get_post_meta( get_the_ID(), 'meta-box-text_ela', true); ?>" title="<?php the_title_attribute(); ?>" >
		<?php the_post_thumbnail('full'); ?></a></div>
        <div class="blog-content" id="text-setting-n">
			
        	<?php 
	$excerpt = get_the_excerpt();
	$excerpt = substr( $excerpt , 0, 200); ?>
	<p><?php echo $excerpt; ?></p>
    
    <a href="<?php echo get_post_meta( get_the_ID(), 'meta-box-text_ela', true); ?>">View Products</a>
    
        </div>
    </div>


<?php endwhile; endif; ?>
</div>

</div>
</div>

<div class="blog-posts-part">
	<div class="heading-blog"><img src="https://www.supercoollearningtools.com/demo/wp-content/uploads/2020/01/blog-title.png" /></div>
	<div class="main-content" style="display:inline-block;">
    
    
    	
        
 <?php       $query = new WP_Query( array( 'post_type' => 'post' ) );
$posts = $query->posts;

foreach($posts as $post) { ?>
	<div class="blog-section-common">
    <div class="blog-image"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
		<?php the_post_thumbnail('full'); ?></a></div>
        <div class="blog-content" id="text-setting-n">
			<h3><?php echo the_title(); ?></h3>
        	<?php 
	$excerpt = get_the_excerpt();
	$excerpt = substr( $excerpt , 0, 200); ?>
	<p><?php echo $excerpt; ?></p>
        </div>
    </div>
<?php }
?>
    </div>
</div>



</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>