<?php
/* Home Products CPT */	
	if ( !function_exists('homeproduct_category_register') ) {
    	function homeproduct_category_register() {
    		$homeproduct_permalinks = get_option( 'homeproduct_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Topics'),
    	        "singular_label" 				=> __('Topic'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
    	        'show_in_nav_menus'             => false,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $homeproduct_permalinks['category_base'] ) ? __( 'homeproduct-category' ) : __( $homeproduct_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'homeproduct-category', 'homeproduct', $args );
    	}
    	add_action( 'init', 'homeproduct_category_register' );
    }
	/* homeproduct POST TYPE
    ================================================== */
    if ( !function_exists('homeproduct_register') ) {
        function homeproduct_register() {
    		$homeproduct_permalinks = get_option( 'homeproduct_permalinks' );
            $homeproduct_permalink  = empty( $homeproduct_permalinks['homeproduct_base'] ) ? __( 'homeproduct' ) : __( $homeproduct_permalinks['homeproduct_base']  );
            $labels = array(
                'name' => __('Home Products'),
                'singular_name' => __('Home Product'),
                'add_new' => __('Add New Product'),
                'add_new_item' => __('Add New Product'),
                'edit_item' => __('Edit Product'),
                'new_item' => __('New Product'),
                'view_item' => __('View Product'),
                'search_items' => __('Search Product'),
                'not_found' =>  __('No Product have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-products',
                'rewrite'           => $homeproduct_permalink != "homeproduct" ? array(
                    'slug'       => untrailingslashit( $homeproduct_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('homeproduct-category', 'post_tag')
            );
            register_post_type( 'homeproduct', $args );
        }
        add_action( 'init', 'homeproduct_register' );
    }
	/* homeproduct POST TYPE COLUMNS
	================================================== */
    if ( !function_exists('homeproduct_edit_columns') ) {
    	function homeproduct_edit_columns($columns){
            $columns = array(
                "cb" => "<input type=\"checkbox\" />",
                "title" => __("Home Products Title"),
               
               // "homeproduct-category" => __("Topics")
            );
            return $columns;
    	}
    	add_filter("manage_edit-homeproduct_columns", "homeproduct_edit_columns");
    }
	
/* Home Products shotcode */
function homeproduct_add(){
ob_start();
?>
<div class="homeproduct alignfull">
<?php
	$args = array(
    'post_type' => 'homeproduct',
	'posts_per_page' => -1,
	'order' => 'ASC'
);
$loop = new WP_Query($args);
while($loop->have_posts()): $loop->the_post();
?>
<div class="Homeproduct-v">

<div class="Homeproduct-img"> <a href="<?php echo get_field('home_product_url'); ?>" target="_self"><?php the_post_thumbnail('full'); ?></a></div>

 

</div>
<?php
endwhile;
?>
</div>
<?php
return ob_get_clean();
}
add_shortcode('homeproduct-display', 'homeproduct_add');

?>
