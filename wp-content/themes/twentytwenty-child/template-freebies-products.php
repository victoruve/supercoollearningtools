<?php
/**
 * Template Name: Freebies Template Products
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */
get_header();
 
?>
<!-- .nav-toggle -->

			 
 

				<div class="custom-page-menu3">
				<div class="custom-page-menu-wrap3">
 
				<?php
				$args = array(
					'type'                     => 'freebies',
					'child_of'                 => 0,
					'parent'                   => '0',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 1,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'taxonomy'                 => 'freebies-category',
					'pad_counts'               => false );
					
				$categories = get_categories($args);
					echo '<ul>';
						foreach ($categories as $category) {
							$url = get_term_link($category);?>
							<li><a href="<?php echo $url;?>"><?php echo $category->name; ?></a></li><?php
						}
					echo '</ul>';
			?>
            
				</div>
				</div><!-- custom-page-menu -->
 

<main id="site-content" role="main" style="clear:both">

<div class="main-content">
	<div class="products-part">


<div class="top-content">
	
    
    <?php

if ( have_posts() ) {

	while ( have_posts() ) {
		the_post();
?>
		<h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
<?php	}
}

?>

</div>






<!--
<div class="sorting">
<script type="text/javascript">
    //http://www.netlobo.com/url_query_string_javascript.html
    function gup( name, default_value )
    {
      name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
      var regexS = "[\\?&]"+name+"=([^&#]*)";
      var regex = new RegExp( regexS );
      var results = regex.exec( window.location.href );
      if( results == null )
        return default_value;
      else
        return results[1];
    }   
    //here you can output default values for sortby and order by your php script
    var orderby = gup("orderby","");
    var order = gup("order","");
    function change_url() {
        var url = "<?php echo $short_page_url; ?>?orderby="+orderby+"&order="+order;
        document.location = url;
    }

    //you still have to let javascript or php select the correct options
</script> 


<div class="sort-n">
<div class="sort-by-n"><label>Sort by</label>  
     <select onChange="if (this.value) { orderby = this.value; change_url(); }">
 	    <option>Select</option>   
        <option value="DESC">Latest first</option>  
        <option value="ASC">Oldest first</option>  
        <option value="title">Titles A-Z</option>  
     </select>
     </div>
<div class="no-of-products-n"><label>Show</label>
     <select onChange="if (this.value) { order = this.value; change_url(); }">  
        <option>Select</option>  
        <option value="-1">All</option>  
        <option value="1">1-12</option>  
        <option value="2">1-24</option>
     </select>
     </div>
</div>

</div>
-->




	<div class="readings-box">
<?php
$sor = $_GET['orderby'];
$oby = $_GET['order'];
$args = array(
    'post_type'=> 'freebies',
    'order'    => $sor,
	'posts_per_page' => $oby
    );
$the_query = new WP_Query( $args );
if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 

?>


	<div class="readings-inner-box">
    	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
    	    <?php the_post_thumbnail('full'); ?>
	    </a>
       <!-- <h3><?php //echo the_title(); ?></h3> -->
    </div>


<?php endwhile; endif; ?>
</div>

</div>
</div>

<div class="blog-posts-part">
	<div class="heading-blog"><img src="https://www.supercoollearningtools.com/demo/wp-content/uploads/2020/01/blog-title.png" /></div>
	<div class="main-content" style="display:inline-block;">
    
    
    	
        
 <?php       $query = new WP_Query( array( 'post_type' => 'post' ) );
$posts = $query->posts;

foreach($posts as $post) { ?>
	<div class="blog-section-common">
    <div class="blog-image"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
		<?php the_post_thumbnail('full'); ?></a></div>
        <div class="blog-content" id="text-setting-n">
			<h3><?php echo the_title(); ?></h3>
        	<?php 
	$excerpt = get_the_excerpt();
	$excerpt = substr( $excerpt , 0, 200); ?>
	<p><?php echo $excerpt; ?></p>
        </div>
    </div>
<?php }
?>
    </div>
</div>



</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>