<?php
/**
 * Template Name: Reading Template Products
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */
get_header();
 
?>
<!-- .nav-toggle -->

			 
 <div class="blog-banner">
 	<div class="blog-main-container">
    	<h2>Blog</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
    </div>
 </div>

<div class="blogs-container">
	<div class="show-all-blog-outter">
    <div class="blog-heading">
    	<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/latest-blog.jpg" />
    </div>
<?php
$query = array( 'posts_per_page' => -1, 'order' => 'ASC' );
$wp_query = new WP_Query($query);

if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


<div class="blog-posts">
	<div class="image-part">
    	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
		<?php the_post_thumbnail('full'); ?></a>
    </div>
    <div class="content-part">
    	<h3><?php echo the_title(); ?></h3>
        <?php 
	$excerpt = get_the_excerpt();
	$excerpt = substr( $excerpt , 0, 500); ?>
	<?php echo $excerpt; ?>
    
    <a href="<?php the_permalink(); ?>">Read More</a>
    </div>
</div>


<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts published so far.'); ?></p>
<?php endif; ?>


    </div>
</div>


<div class="video-container-outter">
	<div class="main-video-box">
	    <div class="blog-heading">
    	<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/video-image.jpg" />
    </div>
    
        <div class="video-container">
        	<iframe width="560" height="315" src="https://www.youtube.com/embed/NpEaa2P7qZI?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    
    </div>
</div>

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>