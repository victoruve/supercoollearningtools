<?php
get_header('new');
?>
<!-- .nav-toggle -->

			 
 <div class="breadcrumb">
	<div class="breadcrumb-container">
		<ul id="breadcrumbs" class="flat-list">
        	<li><a href="<?php echo home_url(); ?>">Home</a></li>
            <li class="separator">/</li>
			<li><a href="<?php echo home_url(); ?>/lessons/">Curriculum</a></li>
            <li class="separator">/</li> 
            <li><a href="<?php echo home_url(); ?>/socials/">Social Studies</a></li> 
            <li class="separator"> / </li> <li class="parent-category-v"> 
		  
<?php
$terms = get_the_terms( get_the_ID(), 'socials-category' );
if ( ! empty( $terms ) ) :
  echo "<ul class='parent-category-root'>";
  // Parent term detection and display
  $term = array_pop( $terms );
  $parent_term = ( $term->parent ? get_term( $term->parent, 'socials-category' ) : $term );
  echo "<li>";
  echo '<a href="' . get_term_link( $parent_term, 'socials-category' ) . '">' . $parent_term->name . '</a>';
  // Getting children
  $child_terms = get_term_children( $parent_term->term_id, 'socials-category' );
  if ( ! empty( $child_terms ) ) :
    echo " <ul class='child-category-v'> <li class='separator'> / </li>  ";
    foreach ( $child_terms as $child_term_id ) :
      $child_term = get_term_by( 'id', $child_term_id, 'socials-category' );
      echo ' <li class="cat-child-1"> <a href="'. get_term_link( $child_term, 'socials-category' ) . '">' . $child_term->name . " </a> </li>";
    endforeach;
    echo "</ul>";
  endif; // ( ! empty( $child_terms ) )
  echo "</li>";
  echo "</ul>";
endif; // ( ! empty( $terms ) )
 
?>     
            </li> 
            <li class="separator"> / </li> 
            <li><strong class="bread-current"><?php the_title(); ?></strong></li>
        </ul>
	</div>
</div>

<main id="site-content" role="main" style="clear:both">
<div class="main-content">
	<div class="products-single-part">


		<div class="upper-box">
        	<div class="col-image">
            	<div class="featured-image">
    	    		<?php the_post_thumbnail('full'); ?>
	    		</div>
                <div class="thumbnails">
<?php 
$image = get_field('socials_image_1');
if( !empty($image) ): ?>
	<a class="example-image-link" href="<?php echo $image['url']; ?>" data-lightbox="example-set"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
<?php endif; ?>
<?php 
$image = get_field('socials_image_2');
if( !empty($image) ): ?>
	<a class="example-image-link" href="<?php echo $image['url']; ?>" data-lightbox="example-set"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
<?php endif; ?>
<?php 
$image = get_field('socials_image_3');
if( !empty($image) ): ?>
	<a class="example-image-link" href="<?php echo $image['url']; ?>" data-lightbox="example-set"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
<?php endif; ?>                    


                </div>
                
            </div><div class="col-short-details">
            	
				<div class="product-heading"><h1><?php echo the_title(); ?></h1></div>

                <div class="short-description"><?php echo get_post_meta( get_the_ID(), 'meta-box-textarea_socials', true); ?></div>
                 <div class="short-description"><?php echo the_excerpt(); ?></div>
                <div class="product-button"> 
                	<a href="<?php echo get_post_meta( get_the_ID(), 'meta-box-text_socials', true); ?>" target="_blank">Buy Now</a>
                </div>
				<div class="product-button-cart">
					<?php 
					$postid = get_post_meta( get_the_ID() , 'meta-box-cart_socials', true);
					echo do_shortcode('[ajax_add_to_cart product='.$postid.'   button_text="Add TO cart" show_price={beginning} ]');
					echo do_shortcode('[yith_wcwl_add_to_wishlist product_id='.$postid.'] ');?>
				</div>
				
            </div>
        </div>
        
        <div class="product-description-box">
       
        <?php
	if ( have_posts() ) {
	while ( have_posts() ) {
	the_post();
?>
	<h4><?php echo the_title(); ?> Full Description</h4>
	<?php echo the_content(); ?>
<?php } } ?>            
        </div>
</div>
</div>

<div class="blog-posts-part">
	<div class="heading-blog"><h3>Related Products</h3></div>
	<div class="main-content" style="display:inline-block;">
    
    
    	
        
<?php
$args = array(
    'post_type'=> 'socials',
    'orderby'    => 'rand',
	'posts_per_page' => 3
    );
$the_query = new WP_Query( $args );
if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 

?>
	<div class="readings-inner-box">
    	<a href="<?php echo get_post_meta( get_the_ID(), 'meta-box-text-socials', true); ?>" title="<?php the_title_attribute(); ?>" >
    	    <?php the_post_thumbnail('full'); ?>
	    </a>
    </div>
<?php endwhile; endif; ?>





    </div>
</div>
</main>

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lightbox-plus-jquery.min.js"></script>
<?php get_footer(); ?>