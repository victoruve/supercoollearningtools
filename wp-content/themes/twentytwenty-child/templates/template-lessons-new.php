<?php
/**
 * Template Name: Lessons New Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */
get_header('new');
 
?>

	<div class="banner">
 	<div class="banner-container">
    	<div id="banner-fade"> 

<ul class="bjqs">

<?php
	$args = array(
    'post_type' => 'slider',
);
$loop = new WP_Query($args);
while($loop->have_posts()): $loop->the_post();
?>
	<li><?php if(has_post_thumbnail()){
                 the_post_thumbnail();
             } else { ?>
             <img src="http://localhost/new-wordpress-site/wp-content/uploads/2020/01/noimage.jpg"  />
             <?php } ?>
    </li>
<?php
endwhile;
?>
</ul>
</div>
    </div>
 </div>		<!-- custom-page-menu -->
 
 
 <div class="breadcrumb" style="margin-top:40px;">
	<div class="breadcrumb-container">
		<ul id="breadcrumbs" class="flat-list">
        	<li><a href="<?php echo home_url(); ?>">Home</a></li>
            <li class="separator">/</li>
            <li><strong class="bread-current">Curriculum</strong></li>
        </ul>
	</div>
</div>
<!-- .nav-toggle -->




<main id="site-content" role="main" style="clear:both" class="ela-page">

<div class="main-content">
	<div class="products-part">


<div class="top-content">
	
    
    <?php

if ( have_posts() ) {

	while ( have_posts() ) {
		the_post();
?>
		<h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
<?php	}
}

?>

</div>











	<div class="readings-box">
<?php
$args = array(
    'post_type'=> 'lessons',
    'order'    => 'ASC',
	'posts_per_page' => 12
    );
$the_query = new WP_Query( $args );
if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 

?>


	<div class="blog-section-common">
    <div class="blog-image"><a href="<?php echo get_post_meta( get_the_ID(), 'meta-box-text_lessons', true); ?>" title="<?php the_title_attribute(); ?>" >
		<?php the_post_thumbnail('full'); ?></a></div>
        <div class="blog-content" id="text-setting-n">
			
        	<?php 
	$excerpt = get_the_excerpt();
	$excerpt = substr( $excerpt , 0, 200); ?>
	<p><?php echo $excerpt; ?></p>
    
    <a href="<?php echo get_post_meta( get_the_ID(), 'meta-box-text_lessons', true); ?>">View Products</a>
    
        </div>
    </div>


<?php endwhile; endif; ?>
</div>

</div>
</div>

<div class="blog-posts-part">
	<div class="heading-blog"><img src="https://www.supercoollearningtools.com/demo/wp-content/uploads/2020/01/blog-title.png" /></div>
	<div class="main-content" style="display:inline-block;">
    
    
    	
        
 <?php       $query = new WP_Query( array( 'post_type' => 'post' ) );
$posts = $query->posts;

foreach($posts as $post) { ?>
	<div class="blog-section-common">
    <div class="blog-image"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
		<?php the_post_thumbnail('full'); ?></a></div>
        <div class="blog-content" id="text-setting-n">
			<h3><?php echo the_title(); ?></h3>
        	<?php 
	$excerpt = get_the_excerpt();
	$excerpt = substr( $excerpt , 0, 200); ?>
	<p><?php echo $excerpt; ?></p>
        </div>
    </div>
<?php }
?>
    </div>
</div>



</main><!-- #site-content -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bjqs-1.3.min.js"></script>
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bjqs.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/demo.css">
 <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick-theme.css">
  <style type="text/css">
.slider {
        width: 50%;
        margin: 0px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }


    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: 1;
    }
    
    .slick-active {
      opacity: 1;
    }

    .slick-current {
      opacity: 1;
    }
  </style>
  
  
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    jQuery(document).on('ready', function() {
	
		jQuery('#banner-fade').bjqs({
		  	height        : 622,
            width         : 944,
            responsive  : true
          });
		  
		  
      jQuery(".regular").slick({
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
		responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
      });
	  
	  
	  jQuery(".regular1").slick({
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
		responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
      });
	  
	  
	jQuery(".green_line .left-arrow img").click(function(){
	    jQuery('.regular1 .slick-prev').click();
	    return false;
	});
	jQuery(".green_line .right-arrow img").click(function(){
	    jQuery('.regular1 .slick-next').click();
	    return false;
	});
	
	
	jQuery(".pink_line .left-arrow img").click(function(){
	    jQuery('.regular .slick-prev').click();
	    return false;
	});
	jQuery(".pink_line .right-arrow img").click(function(){
	    jQuery('.regular .slick-next').click();
	    return false;
	});



	  
    });
</script>

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>