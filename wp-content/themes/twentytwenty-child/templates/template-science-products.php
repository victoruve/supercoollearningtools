<?php
/**
 * Template Name: Science Template Products
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */
get_header('new');
 
?>
<!-- .nav-toggle -->

			 
<div class="breadcrumb">
	<div class="breadcrumb-container">
		<ul id="breadcrumbs" class="flat-list">
        	<li><a href="<?php echo home_url(); ?>">Home</a></li>
            <li class="separator">/</li>
			<li><a href="<?php echo home_url(); ?>/lessons/">Curriculum</a></li>
            <li class="separator">/</li> 
            <li><strong class="bread-current"><?php echo the_title(); ?></strong></li>
        </ul>
        
    </div>
</div>

				<!-- custom-page-menu -->
 

<main id="site-content" role="main" style="clear:both">

<div class="main-content">
	<div class="products-part">


<div class="top-content">
	
    
    <?php

if ( have_posts() ) {

	while ( have_posts() ) {
		the_post();
?>
		<h1><?php the_title(); ?></h1>
        <?php the_content(); ?>
<?php	}
}

?>

</div>






<?php
				$args = array(
					'type'                     => 'science',
					'child_of'                 => 0,
					'parent'                   => '0',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 1,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'taxonomy'                 => 'science-category',
					'pad_counts'               => false );
					
				$categories = get_categories($args);
					echo '';
						foreach ($categories as $category) {
							$url = get_term_link($category);?>
                            
                            <div class="blog-section-common">
                            
                           
    <div class="blog-image">
	<a href="<?php echo $url;?>"><?php
		$image_r = get_field('image', 'readings-category_' . $category->term_id . '' );
		
		if($image_r['url'] ==''){
		echo'No Image found';
		}
		else{
			echo '<img src="' . $image_r['url'] . '" /> ';
		}
    ?>
    </a>
    
    </div>
        <div class="blog-content" id="text-setting-n">
			<!-- <h3><?php //echo $category->name; ?></h3> -->
        	<?php 
	$excerpt = $category->description;
	$excerpt = substr( $excerpt , 0, 200); ?>
    
    <?php if($excerpt ==''){
		echo'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt purus vitae neque placerat, ut malesuada nisl ultricies. Nulla facilisi. Mauris aliquam blandit tempus. Quisque vulputate vulputat</p>';
	}else{
		echo '<p>'.$excerpt.'</p>';
	}?>
    <a href="<?php echo $url;?>">View Products</a>
    
        </div>

                            
                            
                            
                            
                         </div>
                         
							
							<?php
						}
					echo '';
			?>






	

</div>
</div>

<div class="blog-posts-part">
	<div class="heading-blog"><img src="https://www.supercoollearningtools.com/demo/wp-content/uploads/2020/01/blog-title.png" /></div>
	<div class="main-content" style="display:inline-block;">
    
    
    	
        
 <?php       $query = new WP_Query( array( 'post_type' => 'post' ) );
$posts = $query->posts;

foreach($posts as $post) { ?>
	<div class="blog-section-common">
    <div class="blog-image"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
		<?php the_post_thumbnail('full'); ?></a></div>
        <div class="blog-content" id="text-setting-n">
			<h3><?php echo the_title(); ?></h3>
        	<?php 
	$excerpt = get_the_excerpt();
	$excerpt = substr( $excerpt , 0, 200); ?>
	<p><?php echo $excerpt; ?></p>
        </div>
    </div>
<?php }
?>
    </div>
</div>



</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>