<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */
$has_social_menu = has_nav_menu( 'social' );
?>
			<?php
		if ( ! is_page_template( array( 'templates/template-canvas.php' ) ) ) {
		?>
		<footer id="site-footer" role="contentinfo" class="header-footer-group">

				<div class="section-inner">

					<div class="footer-credits">

						<p class="footer-copyright">&copy;
							<?php
							echo date_i18n(
								/* translators: Copyright date format, see https://secure.php.net/date */
								_x( 'Y', 'copyright date format', 'twentytwenty' )
							);
							?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>
						</p><!-- .footer-copyright -->

						<p class="powered-by-wordpress">
							<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentytwenty' ) ); ?>">
								<?php _e( 'Powered by WordPress', 'twentytwenty' ); ?>
							</a>
						</p><!-- .powered-by-wordpress -->

					</div><!-- .footer-credits -->

					
					<a class="to-the-top" href="#site-header">
						<img src="https://www.supercoollearningtools.com/demo/wp-content/uploads/2020/02/up-icon-new.png" alt="To The Top" />
					</a><!-- .to-the-top -->
				</div><!-- .section-inner -->
				<nav aria-label="<?php esc_attr_e( 'Social links', 'twentytwenty' ); ?>" class="footer-social-wrapper">

<ul style="text-align:center" class="social-menu footer-social reset-list-style social-icons fill-children-current-color">

	<?php
	wp_nav_menu(
		array(
			'theme_location'  => 'social',
			'container'       => '',
			'container_class' => '',
			'items_wrap'      => '%3$s',
			'menu_id'         => '',
			'menu_class'      => '',
			'depth'           => 1,
			'link_before'     => '<span class="screen-reader-text">',
			'link_after'      => '</span>',
			'fallback_cb'     => '',
		)
	);
	?>

</ul><!-- .footer-social -->

</nav><!-- .footer-social-wrapper -->
			</footer><!-- #site-footer -->

			<?php } ?>


<script>
jQuery(document).ready(function() {
	jQuery('.page-id-697 h1.entry-title').text('Academy');
	jQuery('.freebies img').wrap('<a href="https://www.supercoollearningtools.com/demo/freebies/"></a>');
	jQuery('.curriculum img').wrap('<a href="https://www.supercoollearningtools.com/demo/lessons/"></a>');
	jQuery('.graphics img').wrap('<a href="https://www.supercoollearningtools.com/demo/shop1/"></a>');
	jQuery('.clipart img').wrap('<a href="https://www.supercoollearningtools.com/demo/clip-art/"></a>');
	jQuery('.yith-wcwl-add-button a').html('<i class="yith-wcwl-icon fa fa-heart"></i>');
	
	jQuery(' .woocommerce-breadcrumb').insertBefore("#primary");
	
	jQuery('<span> / <a class="shop-link-v" href="https://www.supercoollearningtools.com/demo/shop1/">Character Art</a> </span>').insertAfter(".single-product  .woo-bread-wrap-v a:first-child");
	 
	jQuery('<span> / <a class="shop-link-v" href="https://www.supercoollearningtools.com/demo/shop1/">Character Art</a> </span>').insertAfter(".tax-product_cat  .woo-bread-wrap-v a:first-child");
	jQuery('<div class="custom-page-menu3 category-menu-vv" style="background-color:#95cdb4 "><div style="background-color:#95cdb4" class="custom-page-menu-wrap3" ><ul><li><a href="https://www.supercoollearningtools.com/demo/product-category/ela/">ELA</a></li><li><a href="https://www.supercoollearningtools.com/demo/product-category/math/">Math</a></li><li><a href="https://www.supercoollearningtools.com/demo/product-category/science/">Science</a></li><li><a href="https://www.supercoollearningtools.com/demo/product-category/social-studies/">Social Studies</a></li><li><a href="https://www.supercoollearningtools.com/demo/product-category/classroom-mgmt/">Classroom Management</a></li></ul></div></div>').insertAfter(".tax-product_cat .term-description");
		
	jQuery('<span class="icon"></span>').insertBefore(".page-template-template-lessons-new .menu-item-has-children .sub-menu");
	jQuery('.summary.entry-summary form.cart').wrap("<div class='product-button-cart'></div>");	
	
	jQuery('.summary.entry-summary .yith-wcwl-add-to-wishlist').insertAfter(".summary.entry-summary .cart .single_add_to_cart_button.button");	
	
	jQuery('.summary.entry-summary .price').insertAfter(".summary.entry-summary .woocommerce-product-details__short-description");	
	
	jQuery('.single .ss-inline-share-wrapper').insertAfter(".single .product-button-cart");	
	 
	
	jQuery('.post-template-default.single .ss-inline-share-wrapper:first-child').insertAfter(".single .entry-header.header-footer-group");	
	
	jQuery('.page-template-template-clipart-products .clip-art-main-img-v').insertAfter(".page-template-template-clipart-products .custom-page-menu3");	
	
	jQuery('#body-v .yith-wcwl-wishlistexistsbrowse a').text("View");	
	
	
	 
	});
</script>


 


		<?php wp_footer(); ?>





	</body>
</html>
