<?php
get_header();
$landingpageid = get_the_ID();
?>
<button class="toggle nav-toggle mobile-nav-toggle" data-toggle-target=".menu-modal"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
	<span class="toggle-inner">
		<span class="toggle-icon">
			<?php twentytwenty_the_theme_svg( 'ellipsis' ); ?>
		</span>
		<span class="toggle-text"><?php _e( 'Menu', 'twentytwenty' ); ?></span>
	</span>
</button><!-- .nav-toggle -->
<div class="custom-page-menu3">
    <div class="custom-page-menu-wrap3">
    	<?php wp_nav_menu( array( 'theme_location' => 'new-menu' ) ); ?>
	</div>
</div>
                    
<main id="site-content" role="main" style="clear:both">
<div class="products-part">





	<div class="readings-box">

<div class="top-content">
	
    
    <?php

if ( have_posts() ) {

	while ( have_posts() ) {
		the_post();
?>

<div class="readings-inner-box">
    	<a href="<?php echo get_post_meta( get_the_ID(), 'meta-box-text', true); ?>" title="<?php the_title_attribute(); ?>" >
    	    <?php the_post_thumbnail('full'); ?>
	    </a>
       <!-- <h3><?php //echo the_title(); ?></h3> -->
    </div>
    
<?php	}
}

?>

</div>

	


</div>

</div>
</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>