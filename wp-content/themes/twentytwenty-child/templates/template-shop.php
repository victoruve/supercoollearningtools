<?php
/**
 * Template Name: Template Shop
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */
get_header();
 
?>

<div class="breadcrumb">
	<div class="breadcrumb-container">
		<ul id="breadcrumbs" class="flat-list">
        	<li><a href="<?php echo home_url(); ?>">Home</a></li>
            <li class="separator">/</li>
            <li><strong class="bread-current"><?php echo the_title(); ?></strong></li>
        </ul>
        
    </div>
</div>

<!-- .nav-toggle -->

			 
 

				<!-- custom-page-menu -->
 

<main id="site-content" role="main" style="clear:both">
	<div class="main-content" >			
						<h1><?php the_title(); ?></h1>	
						
						<p><?php the_excerpt(); ?></p>
	</div>
	
<div class="custom-page-menu3" >
				<div class="custom-page-menu-wrap3">
 
				<?php
				$args = array(
					'type'                     => 'product',
					'child_of'                 => 0,
					'parent'                   => '0',
					'orderby'                  => 'id',
					'order'                    => 'ASC',
					'hide_empty'               => 0,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'taxonomy'                 => 'product_cat',
					'pad_counts'               => false );
					
				$categories = get_categories($args);
					echo '<ul>';
						foreach ($categories as $category) {
							$url = get_term_link($category);?>
							<li><a href="<?php echo $url;?>"><?php echo $category->name; ?></a></li><?php
						}
					echo '</ul>';
			?>
            
				</div>
				</div>
				
					
<div class="main-content" style="clear:both;">
	<div class="products-part">
	


<div class="top-content">
	
    
    <?php

if ( have_posts() ) {

	while ( have_posts() ) {
		the_post();
?>

        <?php the_content(); ?>
<?php	}
}

?>

</div>


                

</div>
</div>

<div class="blog-posts-part">
	<div class="heading-blog"><img src="https://www.supercoollearningtools.com/demo/wp-content/uploads/2020/01/blog-title.png" /></div>
	<div class="main-content" style="display:inline-block;">
    
    
    	
        
 <?php       $query = new WP_Query( array( 'post_type' => 'post' ) );
$posts = $query->posts;

foreach($posts as $post) { ?>
	<div class="blog-section-common">
    <div class="blog-image"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
		<?php the_post_thumbnail('full'); ?></a></div>
        <div class="blog-content" id="text-setting-n">
			<h3><?php echo the_title(); ?></h3>
        	<?php 
	$excerpt = get_the_excerpt();
	$excerpt = substr( $excerpt , 0, 200); ?>
	<p><?php echo $excerpt; ?></p>
        </div>
    </div>
<?php }
?>
    </div>
</div>



</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>