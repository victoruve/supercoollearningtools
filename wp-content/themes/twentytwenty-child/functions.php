<?php
/* enqueue scripts and style from parent theme */        
function twentytwenty_styles() {
	wp_enqueue_style( 'parent', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'twentytwenty_styles');

// Custom logo.
$logo_width  = 402;
$logo_height = 117;

function twentytwenty_sidebar_registration2() {

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		'before_widget' => '<div class="header-top-v">',
		'after_widget'  => '</div>',
	);
	 
	// Header #1.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Header #1', 'twentytwenty' ),
				'id'          => 'sidebar-3',
				'description' => __( 'Widgets in this area will be displayed above the navigation in header.', 'twentytwenty' ),
			)
		)
	);

}

add_action( 'widgets_init', 'twentytwenty_sidebar_registration2' );

function twentytwenty_sidebar_registration3() {

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		'before_widget' => '<div class="header-top-right">',
		'after_widget'  => '</div>',
	);
	 
	// Header #2.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Header #2', 'twentytwenty' ),
				'id'          => 'sidebar-4',
				'description' => __( 'Widgets in this area will be displayed above the navigation in header.', 'twentytwenty' ),
			)
		)
	);

}

add_action( 'widgets_init', 'twentytwenty_sidebar_registration3' );

/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
function my_header_add_to_cart_fragment( $fragments ) {
 
    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php            
    }
        ?></a><?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );

function register_my_menu() {
	register_nav_menu('new-menu',__( 'New Menu' ));
  }
  add_action( 'init', 'register_my_menu' );

  function register_my_menus() {
	register_nav_menus(
	  array(
		'new-menu' => __( 'New Menu' ),
		'curriculum-menu' => __( 'Curriculum Menu' ),
	  )
	);
  }
  add_action( 'init', 'register_my_menus' );
?>

<?php
/* Readings PART */	
if ( !function_exists('readings_category_register') ) {
    	function readings_category_register() {
    		$readings_permalinks = get_option( 'readings_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
    	        'show_in_nav_menus'             => false,
				'show_admin_column' => true,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $readings_permalinks['category_base'] ) ? __( 'readings-category' ) : __( $readings_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'readings-category', 'readings', $args );
    	}
    	add_action( 'init', 'readings_category_register' );
    }
	/* readings POST TYPE
    ================================================== */
    if ( !function_exists('readings_register') ) {
        function readings_register() {
    		$readings_permalinks = get_option( 'readings_permalinks' );
            $readings_permalink  = empty( $readings_permalinks['readings_base'] ) ? __( 'readings' ) : __( $readings_permalinks['readings_base']  );
            $labels = array(
                'name' => __('Readings'),
                'singular_name' => __('Reading'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New Reading'),
                'edit_item' => __('Edit Reading'),
                'new_item' => __('New Reading'),
                'view_item' => __('View Reading'),
                'search_items' => __('Search Reading'),
                'not_found' =>  __('No Reading have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-editor-paste-word',
                'rewrite'           => $readings_permalink != "readings" ? array(
                    'slug'       => untrailingslashit( $readings_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('readings-category', 'post_tag')
            );
            register_post_type( 'readings', $args );
        }
        add_action( 'init', 'readings_register' );
    }

?>	
<?php
function custom_meta_box_markup_readings($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
		<div class="common-meta-box">
			<label for="meta-box-text">External Product URL</label>
			   <input name="meta-box-text_readings" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_readings", true); ?>">
		</div>
		<div class="common-meta-box">
			<label for="meta-box-text">Add to Cart Product ID</label>
			   <input name="meta-box-cart_readings" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-cart_readings", true); ?>">
		</div>
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
        <textarea name="meta-box-textarea_readings"><?php echo get_post_meta($object->ID, "meta-box-textarea_readings", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_readings()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_readings", "readings", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_readings");


// Storedisplay
function save_custom_meta_box_readings($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "readings";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_readings = "";
	$meta_box_textarea_value_readings = "";
	$meta_box_cart_value_readings = "";

	if(isset($_POST["meta-box-text_readings"]))
    {
        $meta_box_text_value_readings = $_POST["meta-box-text_readings"];
    }   
    update_post_meta($post_id, "meta-box-text_readings", $meta_box_text_value_readings);
	
	if(isset($_POST["meta-box-textarea_readings"]))
    {
        $meta_box_textarea_value_readings = $_POST["meta-box-textarea_readings"];
    }   
    update_post_meta($post_id, "meta-box-textarea_readings", $meta_box_textarea_value_readings);
	if(isset($_POST["meta-box-cart_readings"]))
    {
        $meta_box_cart_value_readings = $_POST["meta-box-cart_readings"];
    }   
    update_post_meta($post_id, "meta-box-cart_readings", $meta_box_cart_value_readings);
	
}

add_action("save_post", "save_custom_meta_box_readings", 10, 10);

// Remove
function remove_custom_field_meta_box_readings()
{
    remove_meta_box("postcustom", "readings", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_readings");
?>


<?php function custom_breadcrumbs() {
       
    // Settings
    $separator          = '/';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'flat-list';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '">' . $post_type_object->labels->name . '</li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '">' . $post_type_object->labels->name . '</li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '">' . $cat_name . '</li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '">' . get_the_title($ancestor) . '</li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '">' . get_the_time('M') . ' Archives</li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
        } else if ( get_query_var('paged') ) {
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
        } else if ( is_search() ) {
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
        } elseif ( is_404() ) {      
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
        echo '</ul>';           
    }
}
?>
<?php
/* ela PART */	
	if ( !function_exists('ela_category_register') ) {
    	function ela_category_register() {
    		$ela_permalinks = get_option( 'ela_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
				'show_admin_column' => true,
    	        'show_in_nav_menus'             => false,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $ela_permalinks['category_base'] ) ? __( 'ela-category' ) : __( $ela_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'ela-category', 'ela', $args );
    	}
    	add_action( 'init', 'ela_category_register' );
    }
	/* ela POST TYPE
    ================================================== */
    if ( !function_exists('ela_register') ) {
        function ela_register() {
    		$ela_permalinks = get_option( 'ela_permalinks' );
            $ela_permalink  = empty( $ela_permalinks['ela_base'] ) ? __( 'ela' ) : __( $ela_permalinks['ela_base']  );
            $labels = array(
                'name' => __('ELA'),
                'singular_name' => __('ELA'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New ELA'),
                'edit_item' => __('Edit ELA'),
                'new_item' => __('New ELA'),
                'view_item' => __('View ELA'),
                'search_items' => __('Search ELA'),
                'not_found' =>  __('No ELA have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-welcome-learn-more',
                'rewrite'           => $ela_permalink != "ela" ? array(
                    'slug'       => untrailingslashit( $ela_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('ela-category', 'post_tag')
            );
            register_post_type( 'ela', $args );
        }
        add_action( 'init', 'ela_register' );
    }

?>	
<?php
function custom_meta_box_markup_ela($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
    <div class="common-meta-box">
		<label for="meta-box-text">Custom Url</label>
        <input name="meta-box-text_ela" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_ela", true); ?>">
    </div>
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
        <textarea name="meta-box-textarea_ela"><?php echo get_post_meta($object->ID, "meta-box-textarea_ela", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_ela()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_ela", "ela", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_ela");


// Storedisplay
function save_custom_meta_box_ela($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "ela";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_ela = "";
	$meta_box_textarea_value_ela = "";

	if(isset($_POST["meta-box-text_ela"]))
    {
        $meta_box_text_value_ela = $_POST["meta-box-text_ela"];
    }   
    update_post_meta($post_id, "meta-box-text_ela", $meta_box_text_value_ela);
	
	if(isset($_POST["meta-box-textarea_ela"]))
    {
        $meta_box_textarea_value_ela = $_POST["meta-box-textarea_ela"];
    }   
    update_post_meta($post_id, "meta-box-textarea_ela", $meta_box_textarea_value_ela);
	
}

add_action("save_post", "save_custom_meta_box_ela", 10, 10);

// Remove
function remove_custom_field_meta_box_ela()
{
    remove_meta_box("postcustom", "ela", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_ela");
?>
<?php
/* writing PART */	
	if ( !function_exists('writing_category_register') ) {
    	function writing_category_register() {
    		$writing_permalinks = get_option( 'writing_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
    	        'show_in_nav_menus'             => false,
				'show_admin_column' => true,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $writing_permalinks['category_base'] ) ? __( 'writing-category' ) : __( $writing_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'writing-category', 'writing', $args );
    	}
    	add_action( 'init', 'writing_category_register' );
    }
	/* writing POST TYPE
    ================================================== */
    if ( !function_exists('writing_register') ) {
        function writing_register() {
    		$writing_permalinks = get_option( 'writing_permalinks' );
            $writing_permalink  = empty( $writing_permalinks['writing_base'] ) ? __( 'writing' ) : __( $writing_permalinks['writing_base']  );
            $labels = array(
                'name' => __('Writing'),
                'singular_name' => __('Writing'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New Writing'),
                'edit_item' => __('Edit Writing'),
                'new_item' => __('New Writing'),
                'view_item' => __('View Writing'),
                'search_items' => __('Search writings'),
                'not_found' =>  __('No writings have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-editor-paste-word',
                'rewrite'           => $writing_permalink != "writing" ? array(
                    'slug'       => untrailingslashit( $writing_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('writing-category', 'post_tag')
            );
            register_post_type( 'writing', $args );
        }
        add_action( 'init', 'writing_register' );
    }

?>	
<?php
function custom_meta_box_markup_writing($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
<div class="common-meta-box">
	<label for="meta-box-text">External Product URL</label>
       <input name="meta-box-text_writing" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_writing", true); ?>">
</div>
<div class="common-meta-box">
	<label for="meta-box-text">Add to Cart Product ID</label>
       <input name="meta-box-cart_writing" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-cart_writing", true); ?>">
</div>
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
        <textarea name="meta-box-textarea_writing"><?php echo get_post_meta($object->ID, "meta-box-textarea_writing", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_writing()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_writing", "writing", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_writing");


// Storedisplay
function save_custom_meta_box_writing($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "writing";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_writing = "";
	$meta_box_textarea_value_writing = "";
	$meta_box_cart_value_writing = "";

	if(isset($_POST["meta-box-text_writing"]))
    {
        $meta_box_text_value_writing = $_POST["meta-box-text_writing"];
    }   
    update_post_meta($post_id, "meta-box-text_writing", $meta_box_text_value_writing);
	
	if(isset($_POST["meta-box-textarea_writing"]))
    {
        $meta_box_textarea_value_writing = $_POST["meta-box-textarea_writing"];
    }   
    update_post_meta($post_id, "meta-box-textarea_writing", $meta_box_textarea_value_writing);
	if(isset($_POST["meta-box-cart_writing"]))
    {
        $meta_box_cart_value_writing = $_POST["meta-box-cart_writing"];
    }   
    update_post_meta($post_id, "meta-box-cart_writing", $meta_box_cart_value_writing);
	
}

add_action("save_post", "save_custom_meta_box_writing", 10, 10);

// Remove
function remove_custom_field_meta_box_writing()
{
    remove_meta_box("postcustom", "writing", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_writing");
?>
 
 
 
 <?php
/* math PART */	
	if ( !function_exists('math_category_register') ) {
    	function math_category_register() {
    		$math_permalinks = get_option( 'math_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
    	        'show_in_nav_menus'             => false,
				'show_admin_column' => true,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $math_permalinks['category_base'] ) ? __( 'math-category' ) : __( $math_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'math-category', 'math', $args );
    	}
    	add_action( 'init', 'math_category_register' );
    }
	/* math POST TYPE
    ================================================== */
    if ( !function_exists('math_register') ) {
        function math_register() {
    		$math_permalinks = get_option( 'math_permalinks' );
            $math_permalink  = empty( $math_permalinks['math_base'] ) ? __( 'math' ) : __( $math_permalinks['math_base']  );
            $labels = array(
                'name' => __('Math'),
                'singular_name' => __('Math'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New Math'),
                'edit_item' => __('Edit Math'),
                'new_item' => __('New Math'),
                'view_item' => __('View Math'),
                'search_items' => __('Search Math'),
                'not_found' =>  __('No Math have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-editor-paste-word',
                'rewrite'           => $math_permalink != "math" ? array(
                    'slug'       => untrailingslashit( $math_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('math-category', 'post_tag')
            );
            register_post_type( 'math', $args );
        }
        add_action( 'init', 'math_register' );
    }

?>	
<?php
function custom_meta_box_markup_math($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
  <div class="common-meta-box">
	<label for="meta-box-text">External Product URL</label>
       <input name="meta-box-text_math" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_math", true); ?>">
</div>
<div class="common-meta-box">
	<label for="meta-box-text">Add to Cart Product ID</label>
       <input name="meta-box-cart_math" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-cart_math", true); ?>">
</div>
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
        <textarea name="meta-box-textarea_math"><?php echo get_post_meta($object->ID, "meta-box-textarea_math", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_math()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_math", "math", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_math");


// Storedisplay
function save_custom_meta_box_math($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "math";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_math = "";
	$meta_box_textarea_value_math = "";
	$meta_box_cart_value_math = "";

	if(isset($_POST["meta-box-text_math"]))
    {
        $meta_box_text_value_math = $_POST["meta-box-text_math"];
    }   
    update_post_meta($post_id, "meta-box-text_math", $meta_box_text_value_math);
	
	if(isset($_POST["meta-box-textarea_math"]))
    {
        $meta_box_textarea_value_math = $_POST["meta-box-textarea_math"];
    }   
    update_post_meta($post_id, "meta-box-textarea_math", $meta_box_textarea_value_math);
	if(isset($_POST["meta-box-cart_math"]))
    {
        $meta_box_cart_value_math = $_POST["meta-box-cart_math"];
    }   
    update_post_meta($post_id, "meta-box-cart_math", $meta_box_cart_value_math);
	
}

add_action("save_post", "save_custom_meta_box_math", 10, 10);

// Remove
function remove_custom_field_meta_box_math()
{
    remove_meta_box("postcustom", "math", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_math");
?>
 
 
<?php
/* clipart PART */	
	if ( !function_exists('clipart_category_register') ) {
    	function clipart_category_register() {
    		$clipart_permalinks = get_option( 'clipart_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
    	        'show_in_nav_menus'             => false,
				'show_admin_column' => true,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $clipart_permalinks['category_base'] ) ? __( 'clipart-category' ) : __( $clipart_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'clipart-category', 'clipart', $args );
    	}
    	add_action( 'init', 'clipart_category_register' );
    }
	/* clipart POST TYPE
    ================================================== */
    if ( !function_exists('clipart_register') ) {
        function clipart_register() {
    		$clipart_permalinks = get_option( 'clipart_permalinks' );
            $clipart_permalink  = empty( $clipart_permalinks['clipart_base'] ) ? __( 'clipart' ) : __( $clipart_permalinks['clipart_base']  );
            $labels = array(
                'name' => __('Clipart'),
                'singular_name' => __('Question'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New Clipart'),
                'edit_item' => __('Edit Clipart'),
                'new_item' => __('New Clipart'),
                'view_item' => __('View Clipart'),
                'search_items' => __('Search Cliparts'),
                'not_found' =>  __('No Cliparts have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-buddicons-topics',
                'rewrite'           => $clipart_permalink != "clipart" ? array(
                    'slug'       => untrailingslashit( $clipart_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('clipart-category', 'post_tag')
            );
            register_post_type( 'clipart', $args );
        }
        add_action( 'init', 'clipart_register' );
    }
?>	
<?php
function custom_meta_box_markup_clipart($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <script>
jQuery(document.body).on('click', '.shift8_portfolio_gallery_close', function(event){
event.preventDefault();
if (confirm('Are you sure you want to remove this image?'))
	{
		var removedImage = jQuery(this).children('img').attr('id');
		var oldGallery = jQuery("#shift8_portfolio_gallery").val();
		var newGallery = oldGallery.replace(','+removedImage,'').replace(removedImage+',','').replace(removedImage,'');
		jQuery(this).parents().eq(1).remove();
		jQuery("#shift8_portfolio_gallery").val(newGallery);
	}
});
	</script>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
    <div class="common-meta-box">
		<label for="meta-box-text">External Product URL</label>
        <input name="meta-box-text_clipart" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_clipart", true); ?>">
    </div>
	
	<div class="common-meta-box">
		<label for="meta-box-text">Add to Cart Product ID</label>
        <input name="meta-box-cart_clipart" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-cart_clipart", true); ?>">
    </div>
	
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
        <textarea name="meta-box-textarea_clipart"><?php echo get_post_meta($object->ID, "meta-box-textarea_clipart", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_clipart()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_clipart", "clipart", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_clipart");


// Storedisplay
function save_custom_meta_box_clipart($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "clipart";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_clipart = "";
	$meta_box_cart_value_clipart = "";
	$meta_box_textarea_value_clipart = "";

	if(isset($_POST["meta-box-text_clipart"]))
    {
        $meta_box_text_value_clipart = $_POST["meta-box-text_clipart"];
    }   
    update_post_meta($post_id, "meta-box-text_clipart", $meta_box_text_value_clipart);
	
	if(isset($_POST["meta-box-cart_clipart"]))
    {
        $meta_box_cart_value_clipart = $_POST["meta-box-cart_clipart"];
    }   
    update_post_meta($post_id, "meta-box-cart_clipart", $meta_box_cart_value_clipart);
	
	if(isset($_POST["meta-box-textarea_clipart"]))
    {
        $meta_box_textarea_value_clipart = $_POST["meta-box-textarea_clipart"];
    }   
    update_post_meta($post_id, "meta-box-textarea_clipart", $meta_box_textarea_value_clipart);
	
}

add_action("save_post", "save_custom_meta_box_clipart", 10, 10);

// Remove
function remove_custom_field_meta_box_clipart()
{
    remove_meta_box("postcustom", "clipart", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_clipart");
?>
<?php
/* freebies PART */	
	if ( !function_exists('freebies_category_register') ) {
    	function freebies_category_register() {
    		$freebies_permalinks = get_option( 'freebies_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
				'show_admin_column' => true,
    	        'show_in_nav_menus'             => false,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $freebies_permalinks['category_base'] ) ? __( 'freebies-category' ) : __( $freebies_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'freebies-category', 'freebies', $args );
    	}
    	add_action( 'init', 'freebies_category_register' );
    }
	/* freebies POST TYPE
    ================================================== */
    if ( !function_exists('freebies_register') ) {
        function freebies_register() {
    		$freebies_permalinks = get_option( 'freebies_permalinks' );
            $freebies_permalink  = empty( $freebies_permalinks['freebies_base'] ) ? __( 'freebies' ) : __( $freebies_permalinks['freebies_base']  );
            $labels = array(
                'name' => __('Freebies'),
                'singular_name' => __('Freebie'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New Freebie'),
                'edit_item' => __('Edit Freebie'),
                'new_item' => __('New Freebie'),
                'view_item' => __('View Freebie'),
                'search_items' => __('Search Freebies'),
                'not_found' =>  __('No Freebies have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-buddicons-replies',
                'rewrite'           => $freebies_permalink != "freebies" ? array(
                    'slug'       => untrailingslashit( $freebies_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                ) : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('freebies-category', 'post_tag')
            );
            register_post_type( 'freebies', $args );
        }
        add_action( 'init', 'freebies_register' );
    }
?>	
<?php
function custom_meta_box_markup_freebies($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
    <div class="common-meta-box">
	<label for="meta-box-text">External Product URL</label>
       <input name="meta-box-text_freebies" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_freebies", true); ?>">
</div>
<div class="common-meta-box">
	<label for="meta-box-text">Add to Cart Product ID</label>
       <input name="meta-box-cart_freebies" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-cart_freebies", true); ?>">
</div>
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
        <textarea name="meta-box-textarea_freebies"><?php echo get_post_meta($object->ID, "meta-box-textarea_freebies", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_freebies()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_freebies", "freebies", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_freebies");


// Storedisplay
function save_custom_meta_box_freebies($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "freebies";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_freebies = "";
	$meta_box_textarea_value_freebies = "";
	$meta_box_cart_value_freebies = "";

	if(isset($_POST["meta-box-text_freebies"]))
    {
        $meta_box_text_value_freebies = $_POST["meta-box-text_freebies"];
    }   
    update_post_meta($post_id, "meta-box-text_freebies", $meta_box_text_value_freebies);
	
	if(isset($_POST["meta-box-textarea_freebies"]))
    {
        $meta_box_textarea_value_freebies = $_POST["meta-box-textarea_freebies"];
    }   
    update_post_meta($post_id, "meta-box-textarea_freebies", $meta_box_textarea_value_freebies);
	if(isset($_POST["meta-box-cart_freebies"]))
    {
        $meta_box_cart_value_freebies = $_POST["meta-box-cart_freebies"];
    }   
    update_post_meta($post_id, "meta-box-cart_freebies", $meta_box_cart_value_freebies);
	
}

add_action("save_post", "save_custom_meta_box_freebies", 10, 10);

// Remove
function remove_custom_field_meta_box_freebies()
{
    remove_meta_box("postcustom", "freebies", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_freebies");
?>
<?php
/* Science PART */	
	if ( !function_exists('science_category_register') ) {
    	function science_category_register() {
    		$science_permalinks = get_option( 'science_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
				'show_admin_column' => true,
    	        'show_in_nav_menus'             => false,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $science_permalinks['category_base'] ) ? __( 'science-category' ) : __( $science_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'science-category', 'science', $args );
    	}
    	add_action( 'init', 'science_category_register' );
    }
	/* science POST TYPE
    ================================================== */
    if ( !function_exists('science_register') ) {
        function science_register() {
    		$science_permalinks = get_option( 'science_permalinks' );
            $science_permalink  = empty( $science_permalinks['science_base'] ) ? __( 'science' ) : __( $science_permalinks['science_base']  );
            $labels = array(
                'name' => __('Science'),
                'singular_name' => __('Science'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New Science'),
                'edit_item' => __('Edit Science'),
                'new_item' => __('New Science'),
                'view_item' => __('View Science'),
                'search_items' => __('Search Science'),
                'not_found' =>  __('No Science have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-admin-site-alt2',
                'rewrite'           => $science_permalink != "science" ? array(
                    'slug'       => untrailingslashit( $science_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('science-category', 'post_tag')
            );
            register_post_type( 'science', $args );
        }
        add_action( 'init', 'science_register' );
    }

?>	
<?php
function custom_meta_box_markup_science($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
   <div class="common-meta-box">
	<label for="meta-box-text">External Product URL</label>
       <input name="meta-box-text_science" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_science", true); ?>">
</div>
<div class="common-meta-box">
	<label for="meta-box-text">Add to Cart Product ID</label>
       <input name="meta-box-cart_science" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-cart_science", true); ?>">
</div>
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
		
        <textarea name="meta-box-textarea_science"><?php echo get_post_meta($object->ID, "meta-box-textarea_science", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_science()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_science", "science", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_science");


// Storedisplay
function save_custom_meta_box_science($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "science";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_science = "";
	$meta_box_textarea_value_science = "";
	$meta_box_cart_value_science = "";

	if(isset($_POST["meta-box-text_science"]))
    {
        $meta_box_text_value_science = $_POST["meta-box-text_science"];
    }   
    update_post_meta($post_id, "meta-box-text_science", $meta_box_text_value_science);
	
	if(isset($_POST["meta-box-textarea_science"]))
    {
        $meta_box_textarea_value_science = $_POST["meta-box-textarea_science"];
    } 
	if(isset($_POST["meta-box-cart_science"]))
    {
        $meta_box_cart_value_science = $_POST["meta-box-cart_science"];
    }   
    update_post_meta($post_id, "meta-box-cart_science", $meta_box_cart_value_science);	
    update_post_meta($post_id, "meta-box-textarea_science", $meta_box_textarea_value_science);
	
}

add_action("save_post", "save_custom_meta_box_science", 10, 10);

// Remove
function remove_custom_field_meta_box_science()
{
    remove_meta_box("postcustom", "science", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_science");
?>


<?php
/* Socials PART */	
	if ( !function_exists('socials_category_register') ) {
    	function socials_category_register() {
    		$socials_permalinks = get_option( 'socials_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
    	        'show_in_nav_menus'             => false,
				'show_admin_column' => true,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $socials_permalinks['category_base'] ) ? __( 'socials-category' ) : __( $socials_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'socials-category', 'socials', $args );
    	}
    	add_action( 'init', 'socials_category_register' );
    }
	/* socials POST TYPE
    ================================================== */
    if ( !function_exists('socials_register') ) {
        function socials_register() {
    		$socials_permalinks = get_option( 'socials_permalinks' );
            $socials_permalink  = empty( $socials_permalinks['socials_base'] ) ? __( 'socials' ) : __( $socials_permalinks['socials_base']  );
            $labels = array(
                'name' => __('Social Studies'),
                'singular_name' => __('Social Studies'),
                'add_new' => __('Add New'),
                'add_new_item' => __('Add New Social Studies'),
                'edit_item' => __('Edit Social Studies'),
                'new_item' => __('New Social Studies'),
                'view_item' => __('View Social Studies'),
                'search_items' => __('Search Social Studies'),
                'not_found' =>  __('No Social Studies have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-editor-paste-word',
                'rewrite'           => $socials_permalink != "socials" ? array(
                    'slug'       => untrailingslashit( $socials_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('socials-category', 'post_tag')
            );
            register_post_type( 'socials', $args );
        }
        add_action( 'init', 'socials_register' );
    }

?>	
<?php
function custom_meta_box_markup_socials($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
<div class="common-meta-box">
	<label for="meta-box-text">External Product URL</label>
       <input name="meta-box-text_socials" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_socials", true); ?>">
</div>
<div class="common-meta-box">
	<label for="meta-box-text">Add to Cart Product ID</label>
       <input name="meta-box-cart_socials" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-cart_socials", true); ?>">
</div>
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
        <textarea name="meta-box-textarea_socials"><?php echo get_post_meta($object->ID, "meta-box-textarea_socials", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_socials()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_socials", "socials", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_socials");


// Storedisplay
function save_custom_meta_box_socials($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "socials";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_socials = "";
	$meta_box_textarea_value_socials = "";
	$meta_box_cart_value_socials = "";

	if(isset($_POST["meta-box-text_socials"]))
    {
        $meta_box_text_value_socials = $_POST["meta-box-text_socials"];
    }   
    update_post_meta($post_id, "meta-box-text_socials", $meta_box_text_value_socials);
	
	if(isset($_POST["meta-box-textarea_socials"]))
    {
        $meta_box_textarea_value_socials = $_POST["meta-box-textarea_socials"];
    }   
    update_post_meta($post_id, "meta-box-textarea_socials", $meta_box_textarea_value_socials);
	if(isset($_POST["meta-box-cart_socials"]))
    {
        $meta_box_cart_value_socials = $_POST["meta-box-cart_socials"];
    }   
    update_post_meta($post_id, "meta-box-cart_socials", $meta_box_cart_value_socials);
	
}

add_action("save_post", "save_custom_meta_box_socials", 10, 10);

// Remove
function remove_custom_field_meta_box_socials()
{
    remove_meta_box("postcustom", "socials", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_socials");
?>
 

<?php
/* classroom PART */	
	if ( !function_exists('classroom_category_register') ) {
    	function classroom_category_register() {
    		$classroom_permalinks = get_option( 'classroom_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
				'show_admin_column' => true,
    	        'show_in_nav_menus'             => false,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $classroom_permalinks['category_base'] ) ? __( 'classroom-category' ) : __( $classroom_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'classroom-category', 'classroom', $args );
    	}
    	add_action( 'init', 'classroom_category_register' );
    }
	/* classroom POST TYPE
    ================================================== */
    if ( !function_exists('classroom_register') ) {
        function classroom_register() {
    		$classroom_permalinks = get_option( 'classroom_permalinks' );
            $classroom_permalink  = empty( $classroom_permalinks['classroom_base'] ) ? __( 'classroom' ) : __( $classroom_permalinks['classroom_base']  );
            $labels = array(
                'name' => __('Classroom Management'),
                'singular_name' => __('Classroom'),
                'add_new' => __('Add New Classroom Management'),
                'add_new_item' => __('Add New Classroom Management'),
                'edit_item' => __('Edit Classroom Management'),
                'new_item' => __('New Classroom Management'),
                'view_item' => __('View Classroom Management'),
                'search_items' => __('Search Classroom Management'),
                'not_found' =>  __('No questions have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-smiley',
                'rewrite'           => $classroom_permalink != "classroom" ? array(
                    'slug'       => untrailingslashit( $classroom_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('classroom-category', 'post_tag')
            );
            register_post_type( 'classroom', $args );
        }
        add_action( 'init', 'classroom_register' );
    }
?>	
<?php
function custom_meta_box_markup_classroom($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
   <div class="common-meta-box">
	<label for="meta-box-text">External Product URL</label>
       <input name="meta-box-text_classroom" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_classroom", true); ?>">
</div>
<div class="common-meta-box">
	<label for="meta-box-text">Add to Cart Product ID</label>
       <input name="meta-box-cart_classroom" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-cart_classroom", true); ?>">
</div>
<?php  
}
function add_custom_meta_box_classroom()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_classroom", "classroom", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_classroom");


// Storedisplay
function save_custom_meta_box_classroom($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "classroom";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_classroom = "";
	$meta_box_textarea_value_classroom = "";
	$meta_box_cart_value_classroom = "";

	if(isset($_POST["meta-box-text_classroom"]))
    {
        $meta_box_text_value_classroom = $_POST["meta-box-text_classroom"];
    }   
    update_post_meta($post_id, "meta-box-text_classroom", $meta_box_text_value_classroom);
	
	if(isset($_POST["meta-box-textarea_classroom"]))
    {
        $meta_box_textarea_value_classroom = $_POST["meta-box-textarea_classroom"];
    }   
    update_post_meta($post_id, "meta-box-textarea_classroom", $meta_box_textarea_value_classroom);
	if(isset($_POST["meta-box-cart_classroom"]))
    {
        $meta_box_cart_value_classroom = $_POST["meta-box-cart_classroom"];
    }   
    update_post_meta($post_id, "meta-box-cart_classroom", $meta_box_cart_value_classroom);
	
}

add_action("save_post", "save_custom_meta_box_classroom", 10, 10);

// Remove
function remove_custom_field_meta_box_classroom()
{
    remove_meta_box("postcustom", "classroom", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_classroom");
?>
<?php
/* grammar PART */	
	if ( !function_exists('grammar_category_register') ) {
    	function grammar_category_register() {
    		$grammar_permalinks = get_option( 'grammar_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
				'show_in_nav_menus'             => false,
				'show_admin_column' => true,
    	        'show_in_nav_menus'             => false,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $grammar_permalinks['category_base'] ) ? __( 'grammar-category' ) : __( $grammar_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'grammar-category', 'grammar', $args );
    	}
    	add_action( 'init', 'grammar_category_register' );
    }
	/* grammar POST TYPE
    ================================================== */
    if ( !function_exists('grammar_register') ) {
        function grammar_register() {
    		$grammar_permalinks = get_option( 'grammar_permalinks' );
            $grammar_permalink  = empty( $grammar_permalinks['grammar_base'] ) ? __( 'grammar' ) : __( $grammar_permalinks['grammar_base']  );
            $labels = array(
                'name' => __('Grammar'),
                'singular_name' => __('Grammar'),
                'add_new' => __('Add New Grammar'),
                'add_new_item' => __('Add New Grammar'),
                'edit_item' => __('Edit Grammar'),
                'new_item' => __('New Grammar'),
                'view_item' => __('View Grammar'),
                'search_items' => __('Search Grammar'),
                'not_found' =>  __('No questions have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-editor-spellcheck',
                'rewrite'           => $grammar_permalink != "grammar" ? array(
                    'slug'       => untrailingslashit( $grammar_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('grammar-category', 'post_tag')
            );
            register_post_type( 'grammar', $args );
        }
        add_action( 'init', 'grammar_register' );
    }
?>	
<?php
function custom_meta_box_markup_grammar($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
    <div class="common-meta-box">
	<label for="meta-box-text">External Product URL</label>
       <input name="meta-box-text_grammar" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_grammar", true); ?>">
</div>
<div class="common-meta-box">
	<label for="meta-box-text">Add to Cart Product ID</label>
       <input name="meta-box-cart_grammar" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-cart_grammar", true); ?>">
</div>
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
        <textarea name="meta-box-textarea_grammar"><?php echo get_post_meta($object->ID, "meta-box-textarea_grammar", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_grammar()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_grammar", "grammar", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_grammar");


// Storedisplay
function save_custom_meta_box_grammar($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "grammar";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_grammar = "";
	$meta_box_textarea_value_grammar = "";
	$meta_box_cart_value_grammar = "";

	if(isset($_POST["meta-box-text_grammar"]))
    {
        $meta_box_text_value_grammar = $_POST["meta-box-text_grammar"];
    }   
    update_post_meta($post_id, "meta-box-text_grammar", $meta_box_text_value_grammar);
	
	if(isset($_POST["meta-box-textarea_grammar"]))
    {
        $meta_box_textarea_value_grammar = $_POST["meta-box-textarea_grammar"];
    }   
    update_post_meta($post_id, "meta-box-textarea_grammar", $meta_box_textarea_value_grammar);
	if(isset($_POST["meta-box-cart_grammar"]))
    {
        $meta_box_cart_value_grammar = $_POST["meta-box-cart_grammar"];
    }   
    update_post_meta($post_id, "meta-box-cart_grammar", $meta_box_cart_value_grammar);
}

add_action("save_post", "save_custom_meta_box_grammar", 10, 10);

// Remove
function remove_custom_field_meta_box_grammar()
{
    remove_meta_box("postcustom", "grammar", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_grammar");
?>
<?php
/* lessons PART */	
	if ( !function_exists('lessons_category_register') ) {
    	function lessons_category_register() {
    		$lessons_permalinks = get_option( 'lessons_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
    	        'show_ui'                       => true,
				'show_admin_column' => true,
    	        'show_in_nav_menus'             => false,
				'show_in_rest' => true,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $lessons_permalinks['category_base'] ) ? __( 'lessons-category' ) : __( $lessons_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'lessons-category', 'lessons', $args );
    	}
    	add_action( 'init', 'lessons_category_register' );
    }
	/* lessons POST TYPE
    ================================================== */
    if ( !function_exists('lessons_register') ) {
        function lessons_register() {
    		$lessons_permalinks = get_option( 'lessons_permalinks' );
            $lessons_permalink  = empty( $lessons_permalinks['lessons_base'] ) ? __( 'lessons' ) : __( $lessons_permalinks['lessons_base']  );
            $labels = array(
                'name' => __('Lessons'),
                'singular_name' => __('Lessons'),
                'add_new' => __('Add New Lesson'),
                'add_new_item' => __('Add New Lesson'),
                'edit_item' => __('Edit Lesson'),
                'new_item' => __('New Lessons'),
                'view_item' => __('View Lessons'),
                'search_items' => __('Search Lessons'),
                'not_found' =>  __('No questions have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-desktop',
                'rewrite'           => $lessons_permalink != "lessons" ? array(
                    'slug'       => untrailingslashit( $lessons_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
					'show_in_rest' => true,
                'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments','categories'),
                'has_archive' => true,
                'taxonomies' => array('lessons-category', 'post_tag')
            );
            register_post_type( 'lessons', $args );
        }
        add_action( 'init', 'lessons_register' );
    }
?>	
<?php
function custom_meta_box_markup_lessons($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
    <style>
    	.common-meta-box{margin-bottom:20px; position:relative;}
		.common-meta-box input[type="text"]{width:100%;}
		.common-meta-box input[type="date"]{width:100%;}
		.common-meta-box textarea{width:100%;}
		.common-meta-box select{width:100%;box-sizing: border-box;}
		.common-meta-box label{font-weight:bold; color:#000000; margin-bottom:5px;display: block;}
		#ui-datepicker-div{z-index:9999 !important;}
    </style>
    <div class="common-meta-box">
		<label for="meta-box-text">Custom Url</label>
        <input name="meta-box-text_lessons" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-text_lessons", true); ?>">
    </div>
    <div class="common-meta-box">
		<label for="meta-box-text">Short Description</label>
        <textarea name="meta-box-textarea_lessons"><?php echo get_post_meta($object->ID, "meta-box-textarea_lessons", true); ?></textarea>
    </div>
<?php  
}
function add_custom_meta_box_lessons()
{
    add_meta_box("demo-meta-box", "Custom Meta Box", "custom_meta_box_markup_lessons", "lessons", "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box_lessons");


// Storedisplay
function save_custom_meta_box_lessons($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "lessons";
    if($slug != $post->post_type)
        return $post_id;
		
	$meta_box_text_value_lessons = "";
	$meta_box_textarea_value_lessons = "";

	if(isset($_POST["meta-box-text_lessons"]))
    {
        $meta_box_text_value_lessons = $_POST["meta-box-text_lessons"];
    }   
    update_post_meta($post_id, "meta-box-text_lessons", $meta_box_text_value_lessons);
	
	if(isset($_POST["meta-box-textarea_lessons"]))
    {
        $meta_box_textarea_value_lessons = $_POST["meta-box-textarea_lessons"];
    }   
    update_post_meta($post_id, "meta-box-textarea_lessons", $meta_box_textarea_value_lessons);
	
}

add_action("save_post", "save_custom_meta_box_lessons", 10, 10);

// Remove
function remove_custom_field_meta_box_lessons()
{
    remove_meta_box("postcustom", "lessons", "normal");
}

add_action("do_meta_boxes", "remove_custom_field_meta_box_lessons");
?>
<?php
/* Slider Post Type */
if ( !function_exists('slider_category_register') ) {
    	function slider_category_register() {
    		$slider_permalinks = get_option( 'slider_permalinks' );
    	    $args = array(
    	        "label" 						=> __('Categories'),
    	        "singular_label" 				=> __('Category'),
    	        'public'                        => true,
    	        'hierarchical'                  => true,
				'show_admin_column' => true,
    	        'show_ui'                       => true,
    	        'show_in_nav_menus'             => false,
    	        'args'                          => array( 'orderby' => 'term_order' ),
    	        'rewrite'           => array(
                    'slug'       => empty( $slider_permalinks['category_base'] ) ? __( 'slider-category' ) : __( $slider_permalinks['category_base']   ),
                    'with_front' => false
                ),
                'query_var'         => true
    	    );
    	    register_taxonomy( 'slider-category', 'slider', $args );
    	}
    	add_action( 'init', 'slider_category_register' );
    }
	/* slider POST TYPE
    ================================================== */
    if ( !function_exists('slider_register') ) {
        function slider_register() {
    		$slider_permalinks = get_option( 'slider_permalinks' );
            $slider_permalink  = empty( $slider_permalinks['slider_base'] ) ? __( 'slider' ) : __( $slider_permalinks['slider_base']  );
            $labels = array(
                'name' => __('slider'),
                'singular_name' => __('Slider'),
                'add_new' => __('Add New Slider'),
                'add_new_item' => __('Add New Slider'),
                'edit_item' => __('Edit Slider'),
                'new_item' => __('New Slider'),
                'view_item' => __('View Sliders'),
                'search_items' => __('Search Slider'),
                'not_found' =>  __('No Sliders have been added yet'),
                'not_found_in_trash' => __('Nothing found in Trash'),
                'parent_item_colon' => ''
            );
            $args = array(
                'labels'            => $labels,
                'public'            => true,
                'show_ui'           => true,
                'show_in_menu'      => true,
                'show_in_nav_menus' => true,
                'menu_icon'=> 'dashicons-groups',
                'rewrite'           => $slider_permalink != "slider" ? array(
                    'slug'       => untrailingslashit( $slider_permalink ),
                    'with_front' => false,
                    'feeds'      => true
                )
                    : false,
               // 'supports' => array('title', 'editor'),
			   'show_in_rest' => true,
			   'supports' => array('title','editor', 'author', 'thumbnail', 'excerpt', 'comments'),

                'has_archive' => true,
                'taxonomies' => array('slider-category', 'post_tag')
            );
            register_post_type( 'slider', $args );
        }
        add_action( 'init', 'slider_register' );
    }
?>





<?php
// Shortcode adding function for Freebies
function freebies_protected(){
ob_start();
				$args = array(
					'type'                     => 'freebies',
					'child_of'                 => 0,
					'parent'                   => '0',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 0,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'taxonomy'                 => 'freebies-category',
					'pad_counts'               => false );
					
				$categories = get_categories($args);
				$image = get_field('image_academy');
					echo '<div class="readings-box">
					
					<div class="blog-section-common">
    <div class="blog-image">
	<a href="'.get_field('academy_link').'"><img src="'.$image['url'].'"></a>
    
    </div>
        <div class="blog-content" id="text-setting-n">
			<h3>'.get_field('academy_title').'</h3>
		    <p>'.get_field('academy_text').'</p>
			<a href="'.get_field('academy_link').'">View Freebies</a>
        </div>
	</div>
	
					';
						foreach ($categories as $category) {
							$url = get_term_link($category);
?>
<div class="blog-section-common">
    <div class="blog-image">
	<a href="<?php echo $url;?>"><?php
		$image_r = get_field('image', 'freebies-category_' . $category->term_id . '' );
		
		if($image_r['url'] ==''){
		echo'No Image found';
		}
		else{
			echo '<img src="' . $image_r['url'] . '" /> ';
		}
    ?>
    </a>
    
    </div>
        <div class="blog-content" id="text-setting-n">
			<h3><?php echo $category->name; ?></h3>
    <?php 
		$excerpt = $category->description;
		$excerpt = substr( $excerpt , 0, 200);
	?>
    <?php
	if($excerpt ==''){
		echo'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt purus vitae neque placerat, ut malesuada nisl ultricies. Nulla facilisi. Mauris aliquam blandit tempus. Quisque vulputate vulputat</p>';
	}else{
		echo '<p>'.$excerpt.'</p>';
	}
	?>
    <a href="<?php echo $url;?>">View Freebies</a>
	</div>
	</div>
<?php
		}
	echo '</div>';
?>
<?php
//ob_get_clean();
return ob_get_clean();
}
add_shortcode('freebies-protected-sc', 'freebies_protected');
?>
<?php
/**
 * Change several of the breadcrumb defaults
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' &#47; ',
            'wrap_before' => '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb"><div class="woo-bread-wrap-v">',
            'wrap_after'  => '</div></nav>',
            'before'      => '',
            'after'       => '',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}
?>
<?php
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs', 20 );


function enable_page_excerpt() {
  add_post_type_support('page', array('excerpt'));
}
add_action('init', 'enable_page_excerpt');
?>
 
<?php
function myprefix_enqueue_scripts() {
  wp_enqueue_script( 
    'my-script',
    get_stylesheet_directory_uri() . '/js/custom.js', 
    array('jquery'),
    '1.0.0',
    true );
}

add_action( 'wp_enqueue_scripts', 'myprefix_enqueue_scripts' );
?>
<?php
 add_post_type_support( 'themes', 'thumbnail' );
 ?>
<?php
require get_stylesheet_directory().'/cpt/home_productcpt.php';
?>
 