<?php
/**
 * Template Name: Academy Template Pages
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */
get_header();
 
?>

<div class="breadcrumb">
	<div class="breadcrumb-container">
		<ul id="breadcrumbs" class="flat-list">
        	<li><a href="<?php echo home_url(); ?>">Home</a></li>
            <li class="separator">/</li>
			 <li><a href="<?php echo home_url(); ?>/freebies/">Freebies</a></li>
            <li class="separator">/</li>
			 <li><a href="<?php echo home_url(); ?>/academy/">Academy</a></li>
            <li class="separator">/</li>
            <li><strong class="bread-current"><?php echo the_title(); ?></strong></li>
        </ul>
        
    </div>
</div>

<main id="site-content" role="main">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );
		}
	}

	?>

</main><!-- #site-content -->
<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
