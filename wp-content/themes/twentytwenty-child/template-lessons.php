<?php
/**
 * Template Name: Lessons Page Template
 * Template Post Type: post, page
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
**/
get_header();
 
?>
<button class="toggle nav-toggle mobile-nav-toggle" data-toggle-target=".menu-modal"  data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
						<span class="toggle-inner">
							<span class="toggle-icon">
								<?php twentytwenty_the_theme_svg( 'ellipsis' ); ?>
							</span>
							<span class="toggle-text"><?php _e( 'Menu', 'twentytwenty' ); ?></span>
						</span>
					</button><!-- .nav-toggle -->

			 
 

				<!-- custom-page-menu -->
 <div class="banner">
 	<div class="banner-container">
    	Banner Go here
    </div>
 </div>
 
 <div class="bottom-banner">
 	<div class="bottom-banner">
    	<div class="banner-box">Bottom 1</div>
        <div class="banner-box">Bottom 2</div>
        <div class="banner-box">Bottom 3</div>
    </div>
 </div>

<main id="site-content" role="main" style="clear:both">

<div class="popular-box">
	<div class="max-container"></div>
</div>

<div class="products-call">
	<div class="main-container"></div>
</div>

<div class="lessons">
	<div class="max-container"></div>
</div>

<div class="lessons-product">
	<div class="max-container">
    
    
    <div class="regular slider">
        <?php
        	$args = array(
            	'post_type'=> 'ela',
            	'orderby'    => 'rand',
            	'posts_per_page' => 6,
            );
            $the_query = new WP_Query( $args );
            if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();     
        ?>
        <div class="lessons-product-box">
          <a href="<?php echo get_post_meta( get_the_ID(), 'meta-box-text', true); ?>" title="<?php the_title_attribute(); ?>" >
                            <?php the_post_thumbnail('full'); ?>
                        </a>
        </div>
        <?php endwhile; endif; ?>
  	</div>
  
  
    </div>
</div>

<div class="blog-posts-part">
	<div class="heading-blog"><img src="https://www.supercoollearningtools.com/demo/wp-content/uploads/2020/01/blog-title.png" /></div>
	<div class="main-content" style="display:inline-block;">
    
    
    	
        
 <?php       $query = new WP_Query( array( 'post_type' => 'post' ) );
$posts = $query->posts;

foreach($posts as $post) { ?>
	<div class="blog-section-common">
    <div class="blog-image"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
		<?php the_post_thumbnail('full'); ?></a></div>
        <div class="blog-content" id="text-setting-n">
			<h3><?php echo the_title(); ?></h3>
        	<?php 
	$excerpt = get_the_excerpt();
	$excerpt = substr( $excerpt , 0, 200); ?>
	<p><?php echo $excerpt; ?></p>
        </div>
    </div>
<?php }
?>
    </div>
</div>



</main><!-- #site-content -->

 <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick-theme.css">
  <style type="text/css">
    html, body {
      margin: 0;
      padding: 0;
    }

    * {
      box-sizing: border-box;
    }

    .slider {
        width: 50%;
        margin: 100px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
      color: black;
    }


    .slick-slide {
      transition: all ease-in-out .3s;
      opacity: .2;
    }
    
    .slick-active {
      opacity: .5;
    }

    .slick-current {
      opacity: 1;
    }
  </style>
  
  
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    jQuery(document).on('ready', function() {
      jQuery(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3
      });
    });
</script>
<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>