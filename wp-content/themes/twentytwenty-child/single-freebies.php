<?php
get_header();
$landingpageid = get_the_ID();
$category = get_queried_object();
$myid = $category->term_id;
// echo $category->term_id;
// echo $category;
// echo $landingpageid;
?>
 
<div class="breadcrumb">
	<div class="breadcrumb-container">
		<ul id="breadcrumbs" class="flat-list">
        	<li><a href="<?php echo home_url(); ?>">Home</a></li>
            <li class="separator">/</li>
            <li><a href="<?php echo home_url(); ?>/freebies">Freebies</a></li>
            <li class="separator">/</li>
			<li class="ela-cat-v <?php the_title(); ?>"><a href="<?php echo home_url(); ?>/freebies">ELA</a></li>
            <li class="separator ela-cat-v <?php the_title(); ?>">/</li>
            <li class="auto-cat-v">
			<?php 
            $terms = get_the_terms($post->ID, 'freebies-category' );
			if ($terms && ! is_wp_error($terms)) :
				$tslugs_arr = array();
				foreach ($terms as $term) {
					$tslugs_arr[] = $term->slug;
				}
				$terms_slug_str = join( " <span class='separator'>/</span> ", $tslugs_arr);
			endif;
			?>
				<?php echo '<a href="'.home_url().'/freebies-category/'.$term->slug.'">'.$terms_slug_str.'</a>'; ?>
            <?php ?>

            </li>
            <li class="separator">/</li>
            <li><strong class="bread-current"><?php the_title(); ?></strong></li>
        </ul>
	</div>
</div>


<main id="site-content" role="main" style="clear:both">
<div class="main-content">
	<div class="products-single-part">


		<div class="upper-box">
        	<div class="col-image">
            	<div class="featured-image">
    	    		<?php the_post_thumbnail('full'); ?>
	    		</div>
                
                <div class="thumbnails">
<?php 
$image = get_field('freebies_image_1');
if( !empty($image) ): ?>
	<a class="example-image-link" href="<?php echo $image['url']; ?>" data-lightbox="example-set"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
<?php endif; ?>
<?php 
$image = get_field('freebies_image_2');
if( !empty($image) ): ?>
	<a class="example-image-link" href="<?php echo $image['url']; ?>" data-lightbox="example-set"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
<?php endif; ?>
<?php 
$image = get_field('freebies_image_3');
if( !empty($image) ): ?>
	<a class="example-image-link" href="<?php echo $image['url']; ?>" data-lightbox="example-set"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
<?php endif; ?>                    


                </div>
                
            </div><div class="col-short-details">
            	
				<div class="product-heading"><h1><?php echo the_title(); ?></h1></div>
				
				<!-- <div class="short-description"><//?php echo get_post_meta( get_the_ID(), 'meta-box-textarea_freebies', true); ?></div>-->
				
               <div class="short-description"><?php echo the_excerpt(); ?></div>
                <div class="product-button">
 
					
					 <a href="<?php echo get_post_meta( get_the_ID(), 'meta-box-text_freebies', true); ?>" download>Download</a>
                </div>
				<div class="product-button-cart">
					<?php 
					$postid = get_post_meta( get_the_ID() , 'meta-box-cart_freebies', true);
					echo do_shortcode('[ajax_add_to_cart product='.$postid.'   button_text="Add TO cart" show_price={beginning} ]');
					echo do_shortcode('[yith_wcwl_add_to_wishlist product_id='.$postid.'] ');?>
				</div>
				
            </div>
        </div>
		
		
		
        
        <div class="product-description-box">
       
        <?php
	if ( have_posts() ) {
	while ( have_posts() ) {
	the_post();
?>
 <h4><?php echo the_title(); ?>  Full Description</h4>
	<?php echo the_content(); ?>
<?php } } ?>            
        </div>
</div>
</div>

<div class="blog-posts-part">
	<div class="heading-blog"><h3>Related Products</h3></div>
	<div class="main-content" style="display:inline-block;">
    
    
    	
        
<?php
$args = array(
    'post_type'=> 'freebies',
    'orderby'    => 'rand',
	'posts_per_page' => 3
    );
$the_query = new WP_Query( $args );
if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 

?>
	<div class="readings-inner-box">
    	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
    	    <?php the_post_thumbnail('full'); ?>
	    </a>
    </div>
<?php endwhile; endif; ?>





    </div>
</div>
</main>

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lightbox-plus-jquery.min.js"></script>
<?php get_footer(); ?>